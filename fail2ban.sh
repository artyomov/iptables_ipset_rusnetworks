#!/bin/bash

echo "Проверить, банить или разбанить?"
select PVER in "Проверить" "Банить" "Разбанить";
do
    case $PVER in
        'Проверить' )
        ACTION='check'
        break;;
        'Банить' )
        ACTION='banip'
        break;;
        'Разбанить' )
        ACTION='unbanip'
        break;;
        * )
        exit
    esac
done

echo "IP:"
read IP

if [ "$ACTION" != "check" ];
then 
    echo "Будет выполнена команда fail2ban-client set sshd ${ACTION} ${IP} Продолжить?"
    select CON in "Yes" "No";
    do
        case $CON in
            Yes )
            fail2ban-client set sshd ${ACTION} ${IP}
            break;;
            * )
            exit
        esac
    done
fi

sleep 1
echo "ipset -L | grep ${IP}"
ipset -L | grep ${IP}
