#!/bin/bash
cd /root/sh
wget -O ripe.db.inetnum.gz ftp://ftp.ripe.net/ripe/dbase/split/ripe.db.inetnum.gz
if [ ! -f /root/sh/ripe.db.inetnum.gz ]; then
    echo "File ripe.db.inetnum.gz not found!"
    exit
fi
gunzip ripe.db.inetnum.gz
rm -f ripe.db.inetnum.gz
echo "Get ripe.db.inetnum lines count..."
c=$(wc -l ripe.db.inetnum)
php ranges.php $c
rm -f ripe.db.inetnum
c=$(wc -l ip.ru.ranges.txt)
php ip2cidr.php $c
rm -f ip.ru.ranges.txt

if [ ! -f /root/ipset.rusnetworks.rules ]; then
    echo "File /root/ipset.rusnetworks.rules not found!"
    exit
fi

echo "Remove rule for rusnetworks"
/sbin/iptables -D INPUT -p tcp -m set --match-set rusnetworks src -m state --state NEW -m multiport --dports 80,443 -j ACCEPT

echo "Remove set rusnetworks from ipset"
ipset -X rusnetworks
echo "Restore set rusnetworks from ipset.rusnetworks.rules"
cat /root/ipset.rusnetworks.rules | ipset restore -!

echo "Add iptables rusnetworks rule"
/sbin/iptables -I INPUT 3 -p tcp -m set --match-set rusnetworks src -m state --state NEW -m multiport --dports 80,443 -j ACCEPT
/sbin/iptables -S | grep rusnetworks
