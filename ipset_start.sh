#!/bin/bash
cat /root/ipset.rusnetworks.rules | ipset restore -!
/sbin/iptables -I INPUT 3 -p tcp -m set --match-set rusnetworks src -m state --state NEW -m multiport --dports 80,443 -j ACCEPT
