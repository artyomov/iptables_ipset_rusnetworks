<?php 
/**
 * download db from ftp://ftp.ripe.net/ripe/dbase/split/ripe.db.inetnum.gz
 */

$handle = @fopen("ripe.db.inetnum", "r");

$ranges = [];

echo "Parsing:\n";

if ($handle) {
    $i = 0;
    $j = 0;
    $rc = 0;
    while (($buffer = fgets($handle, 4096)) !== false) {
        $i++;
        $j++;
        if($j == 100000){
            echo "$i of $argv[1] ".ceil(100/$argv[1]*$i)."%\r";
            $j = 0;
        }
        
        if(substr($buffer,0,7) == 'inetnum'){
            $range = substr($buffer,16,-1);
        }
        
        if(substr($buffer,0,7) == 'country'){
            if(substr($buffer,16,2) == 'RU'){
                $ranges[] = $range;
                $rc++;
            }
        }
    }
    
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    
    fclose($handle);
    
    echo "Total ranges: $rc\n";
    
    if(count($ranges) > 0){
        file_put_contents("ip.ru.ranges.txt", implode("\n", $ranges));
    }
}
