<?php

$handle         = @fopen("ip.ru.ranges.txt", "r");
$fullNetworks   = [];
$fullNetworks[] = "104.192.136.0/21";   // ATLASSIAN PTY LTD
$fullNetworks[] = "69.162.124.226/28";   // uptimerobot.com
$fullNetworks[] = "63.143.42.242/28";   // uptimerobot.com
$fullNetworks[] = "46.137.190.132/32";   // uptimerobot.com
$fullNetworks[] = "122.248.234.23/32";   // uptimerobot.com
$fullNetworks[] = "188.226.183.141/32";   // uptimerobot.com
$fullNetworks[] = "178.62.52.237/32";   // uptimerobot.com
$fullNetworks[] = "54.79.28.129/32";   // uptimerobot.com
$fullNetworks[] = "54.94.142.218/32";   // uptimerobot.com
$fullNetworks[] = "104.131.107.63/32";   // uptimerobot.com
$fullNetworks[] = "54.67.10.127/32";   // uptimerobot.com
$fullNetworks[] = "54.64.67.106/32";   // uptimerobot.com
$fullNetworks[] = "159.203.30.41/32";   // uptimerobot.com
$fullNetworks[] = "46.101.250.135/32";   // uptimerobot.com

$fullNetworks[] = "66.249.64.0/19";     // Googlebot
$fullNetworks[] = "66.102.0.0/20";      // Google Inc
$fullNetworks[] = "104.122.243.227/32"; // letsencrypt
$fullNetworks[] = "64.78.149.164/32";   // letsencrypt

$ips = [];
$i = 0;
$j = 0;
echo "Progress ips2cidr:\n";
if ($handle) {
    while (($buffer = fgets($handle, 1024)) !== false) {
        $i++;
        $j++;
        if($j == 100){
            echo "$i of $argv[1] ".ceil(100/$argv[1]*$i)."%\r";
            $j = 0;
        }
        $ips          = explode(" - ", trim($buffer));
        ip2cidr($ips,$fullNetworks);
    }

    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }

    fclose($handle);

    echo "Total networks: " . count($fullNetworks)."\n";

    if (count($fullNetworks) > 0) {
        file_put_contents("/root/ipset.rusnetworks.rules", "create rusnetworks hash:net family inet hashsize 1024 maxelem 500000\nadd rusnetworks " . implode("\nadd rusnetworks ", $fullNetworks) . "\n");
    }
}

function ip2cidr($ips,&$fullNetworks) {
    
    $num    = ip2long($ips[1]) - ip2long($ips[0]) + 1;
    $bin    = decbin($num);

    $chunk = str_split($bin);
    $chunk = array_reverse($chunk);
    $start = 0;

    while ($start < count($chunk)) {
        if ($chunk[$start] != 0) {
            $start_ip = isset($range) ? long2ip(ip2long($range[1]) + 1) : $ips[0];
            $range    = cidr2ip($start_ip . '/' . (32 - $start));
            $fullNetworks[] = $start_ip . '/' . (32 - $start);
        }
        $start++;
    }
}

function cidr2ip($cidr) {
    $ip_arr = explode('/', $cidr);
    $start  = ip2long($ip_arr[0]);
    $nm     = $ip_arr[1];
    $num    = pow(2, 32 - $nm);
    $end    = $start + $num - 1;
    return array($ip_arr[0], long2ip($end));
}
